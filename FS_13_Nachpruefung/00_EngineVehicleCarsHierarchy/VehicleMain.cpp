#include<string>
#include<iostream>
#include "Vehicles.h"

int main() {
	std::cout << "(a)-------\n";
	Car rs5{"V8", "SG 1234", std::cout};
	ElectroCar tesla{"SZ 007",std::cout};
	Vehicle const &vehi=tesla;

	std::cout << "\n\n\n(b)--------\n";
	rs5.start();
	vehi.start();

	std::cout << "\n\n\n(c)--------------\n";

}


// output::
//
//	(a)-------
//	assembled V8
//	installed engine copy V8
//	manufactured with V8V8 scrapped
//	registered SG 1234
//	Car SG 1234 constructed
//	assembled Electro
//	installed engine copy Electro
//	manufactured with ElectroElectro scrapped
//	registered SZ 007
//	Car SZ 007 constructed
//
//
//
//	(b)--------
//	Car SG 1234 starts it's V8
//	Brumm
//	E-Racer SZ 007 starts it's Electro
//	Wuush
//
//
//
//	(c)--------------
//	Car SZ 007 shredded
//	decommissioned SZ 007
//	 destroyed
//	Electro scrapped
//	Car SG 1234 shredded
//	decommissioned SG 1234
//	 destroyed
//	V8 scrapped
