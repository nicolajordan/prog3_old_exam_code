#include "LRU_List.h"

#include <algorithm>
#include <iostream>
#include <iterator>

int main() {
	lru_list<int> l1{1,2,3,4,5};
	std::copy(l1.begin(), l1.end(), std::ostream_iterator<int>(std::cout, ", "));
	std::cout << "\n\n";
	l1.insert(8);
	std::copy(l1.begin(), l1.end(), std::ostream_iterator<int>(std::cout, ", "));
	std::cout << "\n\n";
	l1.insert(5);
	std::copy(l1.begin(), l1.end(), std::ostream_iterator<int>(std::cout, ", "));
	std::cout << "\n\n";
}

// outupt:
//
//	1, 2, 3, 4, 5,
//
//	8, 1, 2, 3, 4, 5,
//
//	5, 8, 2, 3, 4, 5,
