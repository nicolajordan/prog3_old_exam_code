#ifndef A_H_
#define A_H_

#include <iostream>

using namespace std;

struct Animal {
	void move(){
		cout<< "---" << endl;
	}
	virtual void eat() {
		cout << "mmm" << endl;
	}
	Animal(){
		cout<<"animal born"<<endl;
	}
	~Animal(){
		cout<<"animal died"<<endl;
	}
};

struct Bird : Animal {
	void sing() {
		cout<<"chirp"<<endl;
	}
	virtual void move() {
		cout << "fly" << endl;
	}
	Bird(){
		cout << "bird hatched"<<endl;
	}
	~Bird(){
		cout<<"bird crashed"<<endl;
	}
};
struct Scavenger : Animal {
	void eat(){
		cout<<"schmatz"<<endl;
	}
	Scavenger(){
		cout << "a scavenger is born"<<endl;
	}
	~Scavenger(){
		cout<<"new carrion"<<endl;
	}
};

struct Marabu : Bird, Scavenger {
	virtual void sing() {
		cout << "bubu"<< endl;
	}
	void move() {
		cout << "stalk"<<endl;
	}
	void eat(){
		cout<<"pack pack"<<endl;
	}

	Marabu(){
		cout << "a marabu hatched"<<endl;
	}
	~Marabu(){
		cout<<"marabu deceased"<<endl;
	}
};

struct Vulture : Scavenger, Bird {
	virtual void sing() {
		cout << "cry"<< endl;
	}
	void eat(){
		cout<<"pick pick"<<endl;
	}
	Vulture(){
		cout << "a vulture hatched"<<endl;
	}
	~Vulture(){
		cout<<"vulture crashed"<<endl;
	}
};


#endif /* A_H_ */
