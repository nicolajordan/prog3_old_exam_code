#include "A.h"

int main() {
	cout << "(a)-------------------------" << endl;
	Vulture v;
	Marabu m;
	Bird b(v);
	Scavenger *s(&m);

	cout << endl;
	cout << endl;
	cout << "(b)-------------------------" << endl;
	v.sing();
	v.eat();

	cout << endl;
	cout << endl;
	cout << "(c)-------------------------" << endl;
	b.sing();
	b.move();

	cout << endl;
	cout << endl;
	cout << "(d)-------------------------" << endl;
	s->move();
	s->eat();

	cout << endl;
	cout << endl;
	cout << "(e)-------------------------" << endl;
	s = &v; // ACHTUNG!!!!
	s->move();
	s->eat();

	cout << endl;
	cout << endl;
	cout << "(f)-------------------------" << endl;



}


