#include "SortedVector.h"

template<class ForwardIt>
ForwardIt is_sorted_until(ForwardIt first, ForwardIt last)
{
    if (first != last) {
        ForwardIt next = first;
        while (++next != last) {
            if (*next < *first)
                return next;
            first = next;
        }
    }
    return last;
}

template<typename ForwardIt>
bool is_sorted( ForwardIt first, ForwardIt last ) {
	return is_sorted_until(first, last) == last;
}

int main() {

}
