// Teil (d)
#ifndef COUNTCHARS_H_
#define COUNTCHARS_H_

#include "CharCaseLessLess.h"

#include <map>
#include <iostream>
#include <cctype>

using CharCounters=std::map<char,unsigned,CharCaseLessLess>;

CharCounters countChars(std::istream & in){
	CharCounters keeper{};
	char tmp;
	while(in>>tmp) {
		if (isalpha(tmp))keeper[tmp]++;
	}
	return keeper;
};



#endif /* COUNTCHARS_H_ */
