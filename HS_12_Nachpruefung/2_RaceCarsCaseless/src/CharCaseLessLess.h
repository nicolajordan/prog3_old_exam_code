// Teil (b)

#ifndef CHARCASELESSLESS_H_
#define CHARCASELESSLESS_H_


#include <cctype>

struct CharCaseLessLess{
	bool operator()(const char & l, const char & r)const{
		return ::tolower(l) < ::tolower(r);
	}

};

#endif /* CHARCASELESSLESS_H_ */
