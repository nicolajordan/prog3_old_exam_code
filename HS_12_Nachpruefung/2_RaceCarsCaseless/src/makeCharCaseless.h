#ifndef MAKECHARCASELESS_H_
#define MAKECHARCASELESS_H_

#include <functional>
#include <cctype>

// Teil (e)
std::function<bool(char, char)>
makeCaseLessCharLess(){
	using namespace std::placeholders;
	return bind(std::less<char>{},
			bind(::tolower,_1),
			bind(::tolower,_2)
	);
};



#endif /* MAKECHARCASELESS_H_ */
