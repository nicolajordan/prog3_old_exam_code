#include <iostream> // auch Teil (b)
#include <string>
#include <algorithm>
#include <iterator>
#include <sstream>

// Teil (b) Implementation
template <typename T>
void printLine(T e, std::ostream & out=std::cout){
	out << e << '\n';
}

// ende Teil (b)

int main() {
	using std::cout;
	std::string s{"Racecars"};
	cout << "**0** " << std::oct << s.size() << '\n'; // ** 0 **

	sort(s.begin(),s.end());
	std::string u{};
	unique_copy(s.begin(),s.end(),std::back_inserter(u));
	cout << "**1** " << u.size() << '\n'; // ** 1 **

	auto it1=s.begin();
	cout << "**2** " << *it1 << '\n'; // ** 2 **

	auto it2=u.crbegin();
	cout << "**3** " << *it2++ << '\n'; // ** 3 **
	cout << "**4** " << *++it2 << '\n'; // ** 4 **
	auto it3=u.cend(); it3-=4;
	cout << "**5** " << *it3 << '\n'; // ** 5 **
	it3 -= 3;
	cout << "**6** " << *it3 <<'\n';
	cout << "**7** " << char(*u.begin()+1) << '\n'; // ** 7 **

	// Teil (b) "Testcdoe"
	cout << std::dec; // reset stream
	cout << "\n\nTeil (b)\n";
	printLine('a');
	printLine(10);
	printLine(3.14);

	std::ostringstream out;

	printLine('b', out);
	printLine(11, out);
	if (out.str()=="b\n11\n"){
		printLine("OK");
	} else {
		printLine("ERROR");
	}



}
