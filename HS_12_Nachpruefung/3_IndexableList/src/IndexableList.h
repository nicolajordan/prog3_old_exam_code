#ifndef INDEXABLELIST_H_
#define INDEXABLELIST_H_

#include <list> //vorgegeben
#include <stdexcept>

template<typename T>
class IndexableList { //vorgegeben
	// own additions need to be made in the different sections!!!
	using container=std::list<T>;
	container c;
public:
	IndexableList()=default;
	IndexableList(std::initializer_list<T> li):c{li}{}
	template <typename ITER>
	IndexableList(ITER b, ITER e):c(b,e){}
	using size_type=typename container::size_type;

	size_type size() const {
		return c.size();
	}

	bool empty() const {
		return c.empty();
	}

	T operator[](unsigned const index) const{
		if (!c.empty() && index < c.size()) {
			auto it=c.begin();
			advance(it,index);
			return *it;
		}
		throw std::out_of_range("out of range...");
	}
	void push_back(T e){
		c.push_back(e);
	}

	bool operator==(IndexableList const & rhs) const {
		return rhs.c == c;
	}

	bool operator!=(IndexableList const & rhs) const {
		return !(*this == rhs);
	}
	using iterator = typename container::iterator;
	using const_iterator = typename container::const_iterator;

	iterator begin() { return c.begin(); }
	iterator end() { return c.end(); }
	const_iterator begin() const { return c.begin(); }
	const_iterator end() const { return c.end(); }
	const_iterator cbegin() const { return c.cbegin(); }
	const_iterator cend() const { return c.cend(); }


	iterator erase( iterator pos ) {
		return c.erase(pos);
	}
	iterator erase( iterator first, iterator last ){
		return c.erase(first, last);
	}
};

#endif /* INDEXABLELIST_H_ */
