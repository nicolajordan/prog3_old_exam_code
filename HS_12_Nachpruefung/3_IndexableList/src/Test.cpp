#include "IndexableList.h"

#include "cute.h"
#include "ide_listener.h"
#include "xml_listener.h"
#include "cute_runner.h"

void EmptyIndexableList() {
	IndexableList<int> const li{};
	ASSERT_EQUAL(0,li.size());
	ASSERT(li.empty());
	ASSERT_THROWS(li[0],std::out_of_range);
}

void OneElementIndexableList(){
	IndexableList<char> li{};
	li.push_back('a');
	ASSERT_EQUAL(1,li.size());
	ASSERT(!li.empty());
	ASSERT_EQUAL('a', li[0]);
	ASSERT_THROWS(li[1],std::out_of_range);
}

void initializerListIndexableList(){
	IndexableList<int> li{1,2,3,4,5};
	ASSERT_EQUAL(5,li.size());
	ASSERT_EQUAL(5,li[4]);
}

void EqualityIndexableList(){
	IndexableList<int> l{};
	ASSERT(l==l);
	l.push_back(1);
	IndexableList<int> r{1,2};
	ASSERT(l!=r);
	l.push_back(2);
	ASSERT(l==r);
}

// Tests for 3b
void iterableIndexableList() {
	IndexableList<char> const li{'r','a','c','e'};
	std::string s{li.begin(), li.end()};
	ASSERT_EQUAL("race",s);
	ASSERT_EQUAL(li.size(), s.size());
	ASSERT(equal(li.cbegin(), li.cbegin(), s.begin()));
}

void eraseIteratorPositionFromIndexableList(){
	IndexableList<char> li{'r','a','c','e'};
	auto pos = li.erase(li.begin());
	ASSERT(pos==li.begin());
	ASSERT_EQUAL(3, li.size());
	ASSERT_EQUAL("ace",std::string(li.cbegin(),li.cend()));
}

void iteratorFilledIndexableList(){
	std::string s{"RaceCar"};
	IndexableList<char> li{s.begin(),s.end()};
	ASSERT_EQUAL(s.size(), li.size());
}


void runAllTests(int argc, char const *argv[]){
	cute::suite s;

	s.push_back(CUTE(EmptyIndexableList));
	s.push_back(CUTE(OneElementIndexableList));
	s.push_back(CUTE(initializerListIndexableList));
	s.push_back(CUTE(EqualityIndexableList));
	s.push_back(CUTE(iterableIndexableList));
	s.push_back(CUTE(eraseIteratorPositionFromIndexableList));
	s.push_back(CUTE(iteratorFilledIndexableList));

	cute::xml_file_opener xmlfile(argc,argv);
	cute::xml_listener<cute::ide_listener<> >  lis(xmlfile.out);
	cute::makeRunner(lis,argc,argv)(s, "AllTests");
}

int main(int argc, char const *argv[]){
    runAllTests(argc,argv);
    return 0;
}



